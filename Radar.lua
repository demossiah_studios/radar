local frame = CreateFrame('Frame')

frame.hostiles = {}

frame.autohide = false
frame.expiration = 90
frame.warning = true

--[[ Frame events ]] --

frame:RegisterEvent('ADDON_LOADED')

frame:SetScript('OnEvent', function(self, event, ...)
    if (event == 'ADDON_LOADED') then
        self:RegisterEvent('COMBAT_LOG_EVENT_UNFILTERED')
    elseif (event == 'COMBAT_LOG_EVENT_UNFILTERED') then
        self:OnCombatLogEventUnfiltered(CombatLogGetCurrentEventInfo())
    end
end)

frame:SetScript('OnEnter', function(self)
    self.tooltip = true
    self:SetTooltip(self.hostiles)
end)

frame:SetScript('OnLeave', function(self)
    self.tooltip = false
    GameTooltip:Hide()
end)

function frame:Init()
    self:SetPoint('BOTTOMLEFT', 10, 10)
    self:SetSize(33, 33)

    self.background = self:CreateTexture(nil, 'BACKGROUND')
    self.background:SetPoint('CENTER')
    self.background:SetSize(21, 21)
    self.background:SetTexture('Interface/Icons/Ability_Creature_Cursed_04')

    self.overlay = self:CreateTexture(nil, 'OVERLAY')
    self.overlay:SetPoint('TOPLEFT')
    self.overlay:SetSize(56, 56)
    self.overlay:SetTexture('Interface/Minimap/MiniMap-TrackingBorder')

    self.counter = self:CreateFontString(nil, 'ARTWORK', 'QuestFont_Shadow_Huge')
    self.counter:SetPoint('TOPLEFT', self, 'TOPRIGHT', 3, -6)
    self.counter:SetText(0)

    if (self.autohide) then self:Hide() end

    self.tooltip = false
end

--[[ Frame functions ]] --

function frame:OnCombatLogEventUnfiltered(...)
    local timestamp, _, _, sourceGuid, _, sourceFlags, _, targetGuid, _, targetFlags = ...

    local isSourceHostile = bit.band(sourceFlags, COMBATLOG_OBJECT_REACTION_HOSTILE) > 0
    local isSourcePlayer = bit.band(sourceFlags, COMBATLOG_OBJECT_TYPE_PLAYER) > 0

    if (isSourceHostile and isSourcePlayer) then self:SetHostile(timestamp, sourceGuid) end

    local isTargetHostile = bit.band(targetFlags, COMBATLOG_OBJECT_REACTION_HOSTILE) > 0
    local isTargetPlayer = bit.band(targetFlags, COMBATLOG_OBJECT_TYPE_PLAYER) > 0

    if (isTargetHostile and isTargetPlayer) then self:SetHostile(timestamp, targetGuid) end
end

function frame:SetCounter(hostiles)
    local count = table.getn(hostiles)

    self.counter:SetText(count)
end

function frame:SetHostile(timestamp, guid)
    local class, classSlug, race, raceSlug, sex, name, realm = GetPlayerInfoByGUID(guid)

    self.hostiles[guid] = {
        ['class'] = class,
        ['classSlug'] = classSlug,
        ['name'] = name,
        ['race'] = race,
        ['raceSlug'] = raceSlug,
        ['realm'] = realm,
        ['sex'] = sex,
        ['timestamp'] = timestamp,
    }

    self:SetTicker()
    self:Show()
end

--[[ Ticker functions ]] --

function frame:RemTicker(hostiles)
    local count = table.getn(hostiles)

    if (count == 0) then self.ticker:Cancel() end
    if (count == 0) then self.ticker = nil end
    if (count == 0 and self.autohide) then self:Hide() end
end

function frame:SetTicker()
    if (self.ticker ~= nil) then return end

    self.ticker = C_Timer.NewTicker(1, function()
        local hostiles = {}
        local expiration = time() - self.expiration

        for key, value in pairs(self.hostiles) do
            if (value.timestamp < expiration) then
                self.hostiles[key] = nil
            else
                table.insert(hostiles, value)
            end
        end

        self:SetCounter(hostiles)
        self:SetTooltip(hostiles)
        self:RemTicker(hostiles)
    end)
end

--[[ Tooltip functions ]] --

function frame:GetTooltipNameRealmColor(timestamp)
    local difference = math.max(time() - timestamp, 0)
    local percentage = (difference / self.expiration)

    local base = UnitLevel('player') + 9
    local offset = math.floor(percentage * 20)

    return GetQuestDifficultyColor(base - offset)
end

function frame:SetTooltip(hostiles)
    if (not self.tooltip) then return end

    GameTooltip:Hide()

    GameTooltip:SetOwner(self, 'ANCHOR_NONE')
    GameTooltip:SetPoint('BOTTOMRIGHT', 'UIParent', 'BOTTOMRIGHT', -CONTAINER_OFFSET_X - 13, CONTAINER_OFFSET_Y)
    GameTooltip:AddLine('Radar')

    for key, value in pairs(hostiles) do
        local nameRealm = { value.name }
        local raceClass = { value.race, value.class }

        if (value.realm ~= '') then table.insert(nameRealm, value.realm) end

        local nameRealm = table.concat(nameRealm, '-')
        local nameRealmColor = self:GetTooltipNameRealmColor(value.timestamp)

        local raceClass = table.concat(raceClass, ' ')
        local raceClassColor = RAID_CLASS_COLORS[value.classSlug]

        GameTooltip:AddDoubleLine(nameRealm, raceClass,
            nameRealmColor.r, nameRealmColor.g, nameRealmColor.b,
            raceClassColor.r, raceClassColor.g, raceClassColor.b)
    end

    GameTooltip:Show()
end

frame:Init()
