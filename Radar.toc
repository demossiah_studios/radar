## Interface: 11302
## X-Min-Interface: 11302
## Title: Radar
## Notes: Displays nearby hostile players
## Author: DeMossiah
## SavedVariablesPerCharacter: Radar
## X-Curse-Project-ID: 342912
## Version: @project-version@

Radar.lua
